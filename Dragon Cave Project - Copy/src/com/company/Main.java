package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        System.out.println("\n" + "You are in a land full of dragons. In front of you,");

        System.out.println("you see two caves. In one cave, the dragon is friendly");

        System.out.println("and will share his treasure with you. The other dragon");

        System.out.println("is greedy and hungry and will eat you on sight.");

        System.out.println("Which cave will you go into? (1 or 2)");

        int a =0;
        Scanner input = new Scanner(System.in);
        a = input.nextInt();
        while (a != 1 || a != 2){
            if (a ==1 || a == 2){
                break;
            }
            System.out.println("PLease enter a valid input (1 or 2)");
            a = input.nextInt();

        }
        System.out.println("\033[0;1m" + a);

        int random = (int)(Math.random() * (2-1+1) + 1);

        if (a == random){
            HungaryDragon();
        }
        else {
            FriendlyDragon();
        }
  



    }

    public static void HungaryDragon() {

        System.out.println("You approach the cave..." + "\n" +
                "It is dark and spooky..." + "\n" +
                "A large dragon jumps out in front of you! He opens his jaws and..." + "\n" +
                "Gobbles you down in one bite!");
    }

    public static void FriendlyDragon() {

        System.out.println("You approach the cave..." + "\n" +
                "It is colorfull..." + "\n" +
                "A large dragon jumps out in front of you! and hugs you..." + "\n" +
                "Gobbles you are lucky!");
    }
}
